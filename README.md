# Bake

## Installation

    cargo install --all-features --bin bake --path .

### Bash completion

    bake completion bash > ~/.bake/completion.bash
    echo '. ~/.bake/completion.bash' >> ~ /.bashrc
    . ~/.bake/completion.bash

## Usage

    bake cake test -yr resources/test/recipe.yaml

or

    bake cake test -yr git@gitlab.com:bakery1/bake-test-recipe.git