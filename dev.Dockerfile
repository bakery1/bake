FROM rust:1.50

ENV WORK_USER rust
ENV WORK_UID 1000
ENV WORK_GROUP rust
ENV WORK_GID 1000

ENV HUGO_VERSION 0.80.0
ENV HUGO_DEB=hugo_${HUGO_VERSION}_Linux-64bit.deb

RUN groupadd -g ${WORK_GID} ${WORK_GROUP} \
    && useradd -ms /bin/bash -g ${WORK_GID} -u ${WORK_UID} ${WORK_USER} \
    && apt-get update \
    && apt-get install -y bash-completion \
    && apt-get clean \
    && wget https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_DEB} \
    && dpkg -i ${HUGO_DEB} \
    && rm ${HUGO_DEB} \
    && hugo gen autocomplete --type bash --completionfile /home/${WORK_USER}/hugo.bash \
    && echo ". /home/${WORK_USER}/hugo.bash" >> /home/${WORK_USER}/.bashrc \
    # && rustup default nightly \
    && rustup component add rust-src \
    && rustup component add clippy \
    && rustup component add rustfmt 

WORKDIR /workspace
USER ${WORK_USER}

# Must be installed by the user, otherwise we don't have permissions to run makers
RUN cargo install cargo-make

RUN mkdir /home/${WORK_USER}/.bake