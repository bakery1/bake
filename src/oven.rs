//! # Oven module
//!
//! The oven processes the recipe with its ingredients. In other words:
//!
//! - *get_recipe*: clones the recipe from a local or remote git repository and parses its recipe.yaml
//! - *add_ingredients*: asks for input values (aka. ingredients)
//! - *bake*: generates the desired output directory structure, i.e. the cake
//!
use crate::{
    common::*,
    model::Recipe,
    templating::Processor,
    templating::{hb::HandlebarsProcessor, ValueCollector, Var, Vars},
    Git,
};
use std::collections::LinkedList;
use vfs::VfsPath;

pub struct Oven {
    git: Box<dyn Git>,
}

impl Oven {
    pub fn new(git: Box<dyn Git>) -> Self {
        Self { git }
    }

    pub fn get_recipe(&self, url: &str) -> BakeResult<(VfsPath, Recipe)> {
        let path = self.git.clone(url)?;
        let recipe = Recipe::try_from(
            self.git
                .clone(url)?
                .join(crate::BAKE_RECIPE_FILE_NAME)
                .context(Vfs)?,
        )?;
        Ok((path, recipe))
    }

    pub fn add_ingredients(
        &self,
        recipe: &Recipe,
        collect_fn: ValueCollector,
    ) -> BakeResult<LinkedList<Var>> {
        let mut proc = HandlebarsProcessor::new(handlebars::Handlebars::new());
        proc.register_variables(recipe.ingredients.clone())?;
        proc.collect_values(collect_fn)
    }

    pub fn bake(&self, recipe: &Recipe, ingredients: Vars) -> BakeResult<()> {
        Ok(())
    }
}
