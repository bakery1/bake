use crate::{common::*, model::*};
use serde_yaml::from_reader;
use std::convert::TryFrom;
use vfs::VfsPath;

impl TryFrom<VfsPath> for Recipe {
    type Error = Error;
    fn try_from(path: VfsPath) -> Result<Self, Self::Error> {
        let f = path.open_file().context(Vfs)?;
        let r: Recipe = from_reader(f).context(Yaml).context(Parse)?;
        Ok(r)
    }
}

#[cfg(test)]
mod test {

    use super::*;
    use crate::test::*;

    #[test]
    fn recipe_try_from_path_buf_is_ok() {
        let path = test_resource_dir()
            .join("recipe.yaml")
            .expect("recipe file");
        let result = Recipe::try_from(path);
        assert!(result.is_ok());
    }
}
