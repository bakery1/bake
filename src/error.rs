use std::env::VarError;

pub use snafu::{Backtrace, OptionExt, ResultExt, Snafu};

pub type BakeResult<T> = std::result::Result<T, Error>;

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub")]
pub enum Error {
    #[snafu(display("General"))]
    General,
    #[snafu(display("Env Error: {}", source))]
    Env { source: VarError },
    #[snafu(display("Template Error: {}", msg))]
    Template { msg: String },
    #[snafu(display("Parse Error: {}", source))]
    Parse { source: ParseError },
    #[snafu(display("I/O Error: {}", source))]
    Io { source: std::io::Error },
    #[snafu(display("Terminal Error: {}", source))]
    Term { source: term::Error },
    #[snafu(display("Repository Error: {}", source))]
    Repository { source: RepositoryError },
    #[snafu(display("VFS Error: {}\n{}", source, backtrace))]
    Vfs {
        source: vfs::VfsError,
        backtrace: Backtrace,
    },
}

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub")]
pub enum ParseError {
    #[snafu(display("Yaml Error: {}", source))]
    Yaml { source: serde_yaml::Error },
    #[snafu(display("Scanner Error: {}", source))]
    Scanner { source: scanner_rust::ScannerError },
}

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub")]
pub enum RepositoryError {
    #[snafu(display("Git Error: {}", source))]
    Git { source: git2::Error },
}
