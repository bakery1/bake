// #![feature(box_syntax)]

//! # Bake CLI
//!
//! Self documenting clommand line interface.
//!
//! # Example
//!
//! ```bash
//! bake help
//! ```
//!
extern crate scanner_rust;
extern crate term;
use clap::{Clap, IntoApp};
use std::env;
use yansi::Paint;

#[macro_use]
extern crate log;

mod cmd {

    pub mod prelude {
        pub use bake::{common::*, error::*};
        pub use clap::{App, ArgMatches, Clap, FromArgMatches, IntoApp, ValueHint};
    }
    pub mod cake;
    pub mod completion {
        use super::prelude::*;
        use clap_generate::{generate, generators::*, Generator};
        use std::io;

        #[derive(Clap, Debug)]
        pub struct Completion {
            /// Specifies for which type of shell the completion script is generated.
            /// Must be one of `"bash"`, `"elvish"`, `"fish"`, `"powershell"` or `"zsh"`.
            #[clap(name = "for", possible_values = &["bash", "elvish", "fish", "powershell", "zsh"])]
            generate_for: String,
        }

        impl Completion {
            fn print_completions<G: Generator>(app: &mut App) -> BakeResult<()> {
                generate::<G, _>(app, app.get_name().to_string(), &mut io::stdout());
                Ok(())
            }

            pub async fn run(&self, app: &mut App<'_>) -> BakeResult<()> {
                match self.generate_for.as_str() {
                    "bash" => Self::print_completions::<Bash>(app),
                    "elvish" => Self::print_completions::<Elvish>(app),
                    "fish" => Self::print_completions::<Fish>(app),
                    "powershell" => Self::print_completions::<PowerShell>(app),
                    "zsh" => Self::print_completions::<Zsh>(app),
                    _ => panic!("Unknown generator"),
                }
            }
        }
    }
}

#[derive(Clap, Debug)]
#[clap(name = "bake", author, version, about)]
pub struct Main {
    #[clap(subcommand)]
    cmd: MainCmd,
}

#[derive(Clap, Debug)]
enum MainCmd {
    /// Bake a cake according to the given recipe
    Cake(cmd::cake::Cake),
    /// Generate Shell completion scripts
    Completion(cmd::completion::Completion),
}

#[tokio::main]
async fn main() {
    if env::var_os("RUST_LOG").is_none() {
        env::set_var("RUST_LOG", "info,bake=trace");
    }
    env_logger::init();
    let main = Main::parse();
    let mut app = Main::into_app();

    let result = match main.cmd {
        MainCmd::Cake(cake_cmd) => cake_cmd.run().await,
        MainCmd::Completion(comp_cmd) => comp_cmd.run(&mut app).await,
    };

    if let Err(err) = result {
        trace!(
            "{}{}",
            Paint::red("Inner error: ").bold(),
            Paint::red(&err).bold()
        );
        panic!("Error: {}", err);
    }
}
