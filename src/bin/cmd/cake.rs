use super::prelude::*;
use bake::{
    git::GitClient,
    oven::Oven,
    templating::{DisplayVars, Value, Var, Vars},
};
use scanner_rust::Scanner;
use yansi::Paint;

#[derive(Clap, Debug)]
pub struct Cake {
    /// A url to a git repository containing a recipe.yaml
    #[clap(short = 'r', long = "recipe", value_hint = ValueHint::Url)]
    recipe: String,
    /// Automatically confirm continuation avter values have been collected
    #[clap(short = 'y')]
    auto_continue: bool,
    /// The path to the processed project
    #[clap(name = "path", value_hint = ValueHint::DirPath)]
    path: String,
}

impl Cake {
    pub async fn run(&self) -> BakeResult<()> {
        Self::print_moto();

        let mut t = term::stdout().unwrap();
        print!("Loading recipe {}... ", &self.recipe);
        t.flush().context(Io)?;
        let oven = Oven::new(Box::new(GitClient::new()));

        // getting recipe
        let (_path, recipe) = oven.get_recipe(&self.recipe)?;
        Self::print_check();

        // adding ingredients
        let vars = oven.add_ingredients(&recipe, collect_value_interactively)?;
        Self::print_ingredients(&vars);

        if !self.auto_continue {
            ask_continue()?;
        }

        // bake the cake
        Self::print_baking();
        oven.bake(&recipe, vars)
    }

    fn print_check() {
        print!("{}", Paint::green("\u{2713}\n\n"));
    }

    fn print_ingredients(vars: &Vars) {
        println!(
            "\n{}\n",
            Paint::yellow("Baking recipe with the following values:")
        );

        vars.display_vars();
        println!();
    }

    fn print_baking() {
        println!();
        println!(
            "{}",
            Paint::yellow(
                "\u{1F525}\u{1F525}\u{1F525}    Baking the cake...    \u{1F525}\u{1F525}\u{1F525}"
            )
        );
        println!();
    }

    fn print_moto() {
        println!();
        println!("{}", Paint::blue("\u{1F382} Let's bake a cake! \u{1F382}"));
        println!();
    }
}

pub fn ask_continue() -> BakeResult<()> {
    println!("\n{}", Paint::yellow("Continue (y/n)?"));
    let mut scn = Scanner::new(std::io::stdin());
    scn.skip_whitespaces().context(Scanner).context(Parse)?;
    let choice = scn.next_char().context(Scanner).context(Parse)?;
    match choice {
        Some(s) if s == 'y' => {
            println!();
            Ok(())
        }
        _ => {
            println!("{}", Paint::yellow("Abort."));
            println!();
            Err(Error::General)
        }
    }
}

pub fn collect_value_interactively(var: Var) -> BakeResult<Var> {
    let mut scn = Scanner::new(std::io::stdin());
    let mut new_var = var.clone();
    println!(
        "Please enter the {} {}: ",
        Paint::blue(&var.name).italic(),
        Paint::white(format!("({})", &var.value.unwrap_or_default())).dimmed(),
    );
    let input = scn.next_line().context(Scanner).context(Parse)?;
    let mut t = term::stdout().unwrap();

    match input {
        None => return Err(Error::General),
        Some(i) => {
            if !i.is_empty() {
                new_var.value = Some(Value::String(i));
            } else {
                t.cursor_up().context(Term)?;
                t.carriage_return().context(Term)?;
                println!(
                    "{}",
                    Paint::white(new_var.value.clone().unwrap_or_default()).dimmed()
                );
            }
        }
    }
    Ok(new_var)
}
