//! # Git Module
//!
//!
use crate::common::*;
use git2::{Cred, RemoteCallbacks};
use std::{env, path::Path};
use uuid::Uuid;
use vfs::{PhysicalFS, VfsPath};

pub trait Git {
    fn clone(&self, repo: &str) -> BakeResult<VfsPath>;
}

pub struct GitClient {}

impl GitClient {
    pub fn new() -> Self {
        Self {}
    }
}

impl Git for GitClient {
    /// Clones a git repository using SSH and returns the path to the temp
    /// dir where it was cloned into
    fn clone(&self, repo: &str) -> BakeResult<VfsPath> {
        // Prepare callbacks.
        let mut callbacks = RemoteCallbacks::new();
        callbacks.credentials(|_url, username_from_url, _allowed_types| {
            Cred::ssh_key(
                username_from_url.unwrap(),
                None,
                std::path::Path::new(&format!("{}/.ssh/id_rsa", env::var("HOME").unwrap())),
                None,
            )
        });

        // Prepare fetch options.
        let mut fo = git2::FetchOptions::new();
        fo.remote_callbacks(callbacks);

        // Prepare builder.
        let mut builder = git2::build::RepoBuilder::new();
        builder.fetch_options(fo);

        let uuid = Uuid::new_v4();
        let home_dir = env::var("HOME").context(Env)?;
        let home_fs: VfsPath = PhysicalFS::new(home_dir.clone().into()).into();
        let tmp_dir = home_fs
            .join(".bake")
            .context(Vfs)?
            .join("cache")
            .context(Vfs)?;
        tmp_dir.create_dir_all().context(Vfs)?;
        let path = tmp_dir.join(&uuid.to_string()).context(Vfs)?;

        // Clone the project.
        builder
            .clone(
                repo,
                Path::new(&format!("/home/rust/.bake/cache/{}", uuid.to_string())),
            )
            .context(Git)
            .context(Repository)?;
        Ok(path)
    }
}
