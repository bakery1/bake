// #![feature(box_syntax, box_patterns)]
// #![feature(decl_macro)]
// #![feature(associated_type_defaults)]
// #![feature(unboxed_closures)]
// #![feature(assert_matches)]

pub const BAKE_RECIPE_FILE_NAME: &str = "recipe.yaml";

pub mod common;
pub mod error;
pub mod git;
pub mod model;
pub mod oven;
pub mod parser;
pub mod templating;

pub use git::Git;

#[cfg(test)]
pub mod test {

    use vfs::{PhysicalFS, VfsPath};

    #[allow(dead_code)]
    pub fn test_resource_dir() -> VfsPath {
        let mut res_dir: VfsPath = PhysicalFS::new(env!("CARGO_MANIFEST_DIR").into()).into();
        res_dir
            .join("resources/test")
            .expect("Test resourcec directory")
    }
}
