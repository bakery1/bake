use yansi::Paint;

use crate::{common::*, model::*};
use std::{
    collections::{HashMap, LinkedList},
    fmt::{Debug, Display},
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Var {
    pub name: String,
    pub ingredient: Ingredient,
    pub value: Option<Value>,
}

pub enum Template {
    String { name: String, tpl: String },
    File { name: String, path: String },
}

pub type Vars = LinkedList<Var>;

pub trait DisplayVars {
    fn display_vars(&self);
}

impl DisplayVars for LinkedList<Var> {
    fn display_vars(&self) {
        for var in self.into_iter() {
            println!(
                "{}={}",
                &var.name,
                Paint::green(var.value.clone().unwrap_or_default()).italic(),
            );
        }
    }
}

struct VarsUtils<'a>(&'a Vars);

impl<'a> VarsUtils<'a> {
    fn value_map(&self) -> HashMap<String, String> {
        self.0
            .iter()
            .filter_map(|v| {
                if v.value.is_some() {
                    Some((v.name.clone(), v.value.clone()))
                } else {
                    None
                }
            })
            .map(|e| (e.0, e.1.unwrap().to_string()))
            .into_iter()
            .collect()
    }
}
pub type Values = HashMap<String, Value>;

#[derive(Clone, Serialize, Deserialize, PartialEq)]
pub enum Value {
    String(String),
}

impl From<String> for Value {
    fn from(s: String) -> Self {
        Value::String(s)
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::String(s) => write!(f, "{}", s),
        }
    }
}

impl Debug for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::String(s) => write!(f, "{}", s),
        }
    }
}

impl Default for Value {
    fn default() -> Self {
        Value::String("".into())
    }
}

pub type ValueCollector = fn(Var) -> BakeResult<Var>;

pub trait Processor {
    fn register_variables(&mut self, vars: Vec<Ingredient>) -> BakeResult<()> {
        for var in vars {
            self.register_variable(var)?;
        }
        Ok(())
    }

    /// Registers a variable in the processor
    fn register_variable(&mut self, var: Ingredient) -> BakeResult<()>;

    fn collect_values(&mut self, collect_fn: ValueCollector) -> BakeResult<Vars>;
}

pub mod hb {

    use super::{Processor, Value, Var, Vars, VarsUtils};
    use crate::{common::*, model::*};
    use handlebars::Handlebars;
    use std::{collections::LinkedList, fmt::Debug};

    pub struct HandlebarsProcessor<'reg> {
        hbs: Handlebars<'reg>,
        vars: Vars,
    }

    impl<'reg> HandlebarsProcessor<'reg> {
        pub fn new(hbs: Handlebars<'reg>) -> Self {
            Self {
                hbs,
                vars: LinkedList::new(),
            }
        }

        fn process_value<T: Serialize + Debug>(
            &self,
            value: Option<Value>,
            data: &T,
        ) -> Option<Value> {
            match value {
                Some(Value::String(s)) => {
                    let render = self.hbs.render_template(&s, data);
                    match render {
                        Ok(value) => Some(Value::String(value.clone())),
                        _ => None,
                    }
                }
                _ => None,
            }
        }
    }

    impl<'reg> Processor for HandlebarsProcessor<'reg> {
        fn register_variable(&mut self, var: Ingredient) -> BakeResult<()> {
            match &var {
                Ingredient::String {
                    name,
                    value,
                    skip: _,
                } => {
                    let var_entry = Var {
                        name: name.clone(),
                        ingredient: var.clone(),
                        value: value.clone().map(|s| Value::String(s)),
                    };
                    self.vars.push_back(var_entry);
                }
            }

            Ok(())
        }

        fn collect_values(&mut self, collect_fn: super::ValueCollector) -> BakeResult<super::Vars> {
            let mut mut_vars = self.vars.clone();
            let values = &mut VarsUtils(&self.vars).value_map();
            for var in mut_vars.iter_mut() {
                var.value = self.process_value(var.value.clone(), &values.clone());
                let collected = collect_fn(var.clone());
                match collected {
                    Err(e) => return Err(e),
                    Ok(v) => var.value = v.value,
                }
                if var.value.is_some() {
                    values.insert(var.name.clone(), var.value.clone().unwrap().to_string());
                }
            }
            self.vars = mut_vars;
            Ok(self.vars.clone())
        }
    }

    #[cfg(test)]
    mod test {
        use super::*;
        use crate::{templating::VarsUtils, test::test_resource_dir};

        #[test]
        fn register_variables_is_ok() {
            let mut proc = HandlebarsProcessor::new(Handlebars::new());
            let test_recp = Recipe::try_from(
                test_resource_dir()
                    .join("recipe.yaml")
                    .expect("recipe file"),
            )
            .unwrap();
            let result = proc.register_variables(test_recp.ingredients);
            assert!(result.is_ok());
        }

        #[test]
        fn register_variables_pushes_variables_to_processor() {
            let mut proc = HandlebarsProcessor::new(Handlebars::new());
            let test_recp = Recipe::try_from(
                test_resource_dir()
                    .join("recipe.yaml")
                    .expect("recipe file"),
            )
            .unwrap();
            let result = proc.register_variables(test_recp.ingredients.clone());
            assert!(result.is_ok());
            assert_eq!(test_recp.ingredients.len(), 4);
            assert_eq!(proc.vars.len(), test_recp.ingredients.len());
        }

        #[test]
        fn collect_values_pushes_4_variables_with_values() {
            let mut proc = HandlebarsProcessor::new(Handlebars::new());
            let test_recp = Recipe::try_from(
                test_resource_dir()
                    .join("recipe.yaml")
                    .expect("recipe file"),
            )
            .unwrap();
            let result = proc.register_variables(test_recp.ingredients.clone());
            assert!(result.is_ok());
            let result = proc.collect_values(|var| -> BakeResult<Var> {
                let mut collected = var.clone();
                match var.name.as_str() {
                    "project_name" => collected.value = Some(Value::String("test-project".into())),
                    _ => (),
                }
                Ok(collected)
            });
            assert!(result.is_ok());
            let values = VarsUtils(&proc.vars).value_map();
            assert_eq!(values.len(), 4);
            let image = values.get("image").unwrap().clone();
            assert_eq!(image, "docker.io/test-project:0.0.1".to_string());
        }
    }
}
