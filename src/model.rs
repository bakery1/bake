use crate::common::*;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Recipe {
    pub ingredients: Vec<Ingredient>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(untagged)]
pub enum Ingredient {
    String {
        name: String,
        value: Option<String>,
        skip: Option<bool>,
    },
}
